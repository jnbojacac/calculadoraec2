import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Result } from '../class/result';
import { Request } from '../class/request';

@Injectable()
export class CalculadoraService {
  private urlEndPoint: string = "http://ec2-18-117-108-78.us-east-2.compute.amazonaws.com:3000/";
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(private http: HttpClient,
    private router: Router) { }
  
  private addAuthorizationHeaders() {
    this.httpHeaders.append("Content-Length", "50");
    this.httpHeaders.append("User-Agent", "Cliente John");
    return this.httpHeaders;
  }

  Sumar(request: Request): Observable<Result> {
    debugger;
    return this.http.post<Result>(this.urlEndPoint + "suma", request, { headers: this.addAuthorizationHeaders() })
      .pipe(
        catchError(e => {
          debugger;
          if (e.status == 400) {
            return throwError(e);
          }

          return throwError(e);
        })
      );
  }

  Restar(request: Request): Observable<Result> {
    debugger;
    return this.http.post<Result>(this.urlEndPoint + "resta", request, { headers: this.addAuthorizationHeaders() })
      .pipe(
        catchError(e => {
          debugger;
          if (e.status == 400) {
            return throwError(e);
          }

          return throwError(e);
        })
      );
  }

  Multiplicar(request: Request): Observable<Result> {
    debugger;
    return this.http.post<Result>(this.urlEndPoint + "multiplicacion", request, { headers: this.addAuthorizationHeaders() })
      .pipe(
        catchError(e => {
          debugger;
          if (e.status == 400) {
            return throwError(e);
          }

          return throwError(e);
        })
      );
  }

  Dividir(request: Request): Observable<Result> {
    debugger;
    return this.http.post<Result>(this.urlEndPoint + "division", request, { headers: this.addAuthorizationHeaders() })
      .pipe(
        catchError(e => {
          debugger;
          if (e.status == 400) {
            return throwError(e);
          }

          return throwError(e);
        })
      );
  }
}
