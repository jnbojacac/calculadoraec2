import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { CalculadoraService } from '../Services/calculadoraService';
import { Result } from '../class/result';
import { Request } from '../class/request';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Calculadora';
  request: Request = new Request();

  constructor(private calculadoraService: CalculadoraService, private router: Router)
  { }

  suma(num1: String, num2: String): void {
    debugger;
    if (num1 == "" || num2 == "") {
      Swal.fire({
        icon: 'error',
        title: 'Error',
        text: 'Numero 1 y Numero 2 son Campos requeridos',
      });
      return;
    }

    this.request.Num1 = Number(num1);
    this.request.Num2 = Number(num2);

    this.calculadoraService.Sumar(this.request).subscribe(response => {
      Swal.fire({
        icon: 'info',
        title: 'Resultado',
        text: `La suma de ${num1} y ${num2} es: ${response.resultado} `,
      });
    }, () => {
      Swal.fire({
        icon: 'error',
        title: 'Error en Operación',
        text: 'No se pudo efectuar la operación de Suma',
      });
    });    
  }

  resta(num1: String, num2: String): void {
    debugger;
    if (num1 == "" || num2 == "") {
      Swal.fire({
        icon: 'error',
        title: 'Error',
        text: 'Numero 1 y Numero 2 son Campos requeridos',
      });
      return;
    }

    this.request.Num1 = Number(num1);
    this.request.Num2 = Number(num2);

    this.calculadoraService.Restar(this.request).subscribe(response => {
      Swal.fire({
        icon: 'info',
        title: 'Resultado',
        text: `La resta de ${num1} y ${num2} es: ${response.resultado} `,
      });
    }, () => {
      Swal.fire({
        icon: 'error',
        title: 'Error en Operación',
        text: 'No se pudo efectuar la operación de Resta',
      });
    });    
  }

  multi(num1: String, num2: String): void {
    debugger;
    if (num1 == "" || num2 == "") {
      Swal.fire({
        icon: 'error',
        title: 'Error',
        text: 'Numero 1 y Numero 2 son Campos requeridos',
      });
      return;
    }

    this.request.Num1 = Number(num1);
    this.request.Num2 = Number(num2);

    this.calculadoraService.Multiplicar(this.request).subscribe(response => {
      Swal.fire({
        icon: 'info',
        title: 'Resultado',
        text: `La Multiplicación de ${num1} y ${num2} es: ${response.resultado} `,
      });
    }, () => {
      Swal.fire({
        icon: 'error',
        title: 'Error en Operación',
        text: 'No se pudo efectuar la operación de Multiplicación',
      });
    });    
  }

  divi(num1: String, num2: String): void {
    debugger;
    if (num1 == "" || num2 == "") {
      Swal.fire({
        icon: 'error',
        title: 'Error',
        text: 'Numero 1 y Numero 2 son Campos requeridos',
      });
      return;
    }

    this.request.Num1 = Number(num1);
    this.request.Num2 = Number(num2);

    this.calculadoraService.Dividir(this.request).subscribe(response => {
      Swal.fire({
        icon: 'info',
        title: 'Resultado',
        text: `La división de ${num1} y ${num2} es: ${response.resultado} `,
      });
    }, () => {
      Swal.fire({
        icon: 'error',
        title: 'Error en Operación',
        text: 'No se pudo efectuar la operación de Division',
      });
    });    
  }
}
